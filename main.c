/*
 * Save as `main.c` and compile:
 *
 *      make main
 *
 * The run:
 *
 *      ./main multiple words written down to the circular buffer
 *
 * */

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define CB_NFREE(cb) (cb->end >= cb->start\
        ? cb->size - (cb->end - cb->start) - 1\
        : cb->start - cb->end - 1)
#define CB_NOCCUPIED(cb) (cb->end >= cb->start\
        ? cb->end - cb->start\
        : cb->size - (cb->start - cb->end))

struct circbuf {
    size_t start;
    size_t end;
    size_t size;
    int8_t *buf;
};

static inline size_t minsz(size_t a, size_t b) { return a < b ? a : b; }

static size_t circbuf_write(struct circbuf *cb, void *data, size_t nbytes)
{
    if (!(cb && cb->buf && (cb->size > 1) && data))
        return 0;

    size_t written = 0;

    while (nbytes && CB_NFREE(cb)) {
        size_t delta;

        if (cb->end >= cb->start) {
            if (cb->start == 0) {
                delta = minsz(nbytes, cb->size - cb->end - 1);
            } else {
                delta = minsz(nbytes, cb->size - cb->end);
            }
        } else {
            delta = minsz(nbytes, cb->start - cb->end - 1);
        }

        memcpy(&cb->buf[cb->end], &((int8_t *)data)[written], delta);

        nbytes -= delta;
        cb->end += delta;
        written += delta;

        if (cb->end == cb->size) {
            cb->end = 0;
        }
    }

    return written;
}

static size_t circbuf_read(struct circbuf *cb, const void *obuf, size_t bufsz)
{
    if (!(cb && cb->buf && (cb->size > 1) && obuf && bufsz))
        return 0;

    size_t nread = 0;

    while (nread < bufsz && CB_NOCCUPIED(cb)) {
        size_t delta;

        if (cb->end >= cb->start) {
            delta = minsz(bufsz - nread, cb->end - cb->start);
        } else {
            delta = minsz(bufsz - nread, cb->size - cb->start);
        }

        memcpy(&((int8_t *)obuf)[nread], &cb->buf[cb->start], delta);

        cb->start += delta;
        nread += delta;

        if (cb->start == cb->size) {
            cb->start = 0;
        }
    }

    return nread;
}

static void cbdumper_putchar(const struct circbuf *cb, size_t ptr)
{
    if (cb->end >= cb->start) {
        if (ptr >= cb->start && ptr < cb->end) {
            if (cb->buf[ptr] < ' ' || cb->buf[ptr] < 0) {
                putchar(' ');
            } else {
                putchar(cb->buf[ptr]);
            }
        } else {
            putchar('.');
        }
    } else {
        if (ptr >= cb->start || ptr < cb->end) {
            if (cb->buf[ptr] < ' ' || cb->buf[ptr] < 0) {
                putchar(' ');
            } else {
                putchar(cb->buf[ptr]);
            }
        } else {
            putchar('.');
        }
    }
}

static void cbdumper_metadata_putchar(const struct circbuf *cb, size_t ptr)
{
    if (ptr == cb->start && ptr == cb->end) {
        putchar('B');
    } else if (ptr == cb->start) {
        putchar('S');
    } else if (ptr == cb->end) {
        putchar('E');
    } else {
        putchar(' ');
    }
}

static void circbuf_dump(const struct circbuf *cb)
{
    if (!(cb && cb->buf && (cb->size > 1)))
        return;

    size_t ptr;

    printf("\r[");
    ptr = 0;
    while (ptr < cb->size) {
        cbdumper_putchar(cb, ptr++);
    }
    printf("]\n\r ");
    ptr = 0;
    while (ptr < cb->size) {
        cbdumper_metadata_putchar(cb, ptr++);
    }
    printf(" \n\r");
}

int main(int argc, char *const argv[])
{
    size_t ret;
    int8_t tmp[50] = {0};
    int8_t buf[20] = {0};
    struct circbuf cbuf = (struct circbuf){
        .start = 0,
        .end = 0,
        .size = sizeof(buf),
        .buf = buf,
    };

    printf("Initial buffer state:\n");
    circbuf_dump(&cbuf);

    for (int i = 1; i < argc; i++) {
        printf("Writing \"%s\" to circular buffer... ", argv[i]);
        ret = circbuf_write(&cbuf, argv[i], strlen(argv[i]));
        printf("%zd bytes written\n", ret);
        circbuf_dump(&cbuf);

        printf("Draining from circular buffer... ");
        ret = circbuf_read(&cbuf, tmp, sizeof(tmp) - 1);
        printf("%zd bytes read\n", ret);
        circbuf_dump(&cbuf);
        printf("string read from circular buffer: \"%s\"\n", tmp);
    }
}
